class AddNoteToScreenshots < ActiveRecord::Migration
  def up
	add_column :screenshots, :note, :string
  end

  def down
	remove_column :screenshots, :note
  end
end
