class AddSeqToFeatures < ActiveRecord::Migration
  def up
    add_column :features, :seq, :integer, :default => 0
    
  end

  def down
    remove_column :features, :seq
  end
end
