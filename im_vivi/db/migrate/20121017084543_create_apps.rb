class CreateApps < ActiveRecord::Migration
  def up
    create_table :apps do |t|
      t.string :kind
      t.string :features
      t.string :supportedDevices
      t.string :isGameCenterEnabled
      t.string :artistViewUrl
      t.string :artworkUrl60
      t.string :screenshotUrls
      t.string :ipadScreenshotUrls
      t.string :artworkUrl512
      t.string :artistId
      t.string :artistName
      t.string :price
      t.string :version
      t.string :description
      t.string :genreIds
      t.string :releaseDate
      t.string :sellerName
      t.string :currency
      t.string :genres
      t.string :bundleId
      t.string :trackId
      t.string :trackName
      t.string :primaryGenreName
      t.string :primaryGenreId
      t.string :releaseNotes
      t.string :wrapperType
      t.string :trackCensoredName
      t.string :trackViewUrl
      t.string :contentAdvisoryRating
      t.string :artworkUrl100
      t.string :languageCodesISO2A
      t.string :fileSizeBytes
      t.string :formattedPrice
      t.string :averageUserRatingForCurrentVersion
      t.string :userRatingCountForCurrentVersion
      t.string :trackContentRating
      t.string :averageUserRating
      t.string :userRatingCount
    end
  end

  def down
    drop_table :apps
  end
end
