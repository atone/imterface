class AddIpAndSsidsToHistroy < ActiveRecord::Migration
  def up
  	add_column :histories, :ip, :string
  	add_column :histories, :ssids, :string
  end

  def down
  	remove_column :histories, :ip
  	remove_column :histories, :ssids
  end
end
