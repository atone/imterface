class CreateTops < ActiveRecord::Migration
  def up
    create_table :tops do |t|
      t.string :name
      t.string :description
    end
  end

  def down
    drop_table :tops
  end
end
