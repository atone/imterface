class CreateFeatures < ActiveRecord::Migration
  def up
    create_table :features do |t|
      t.string :name
      t.boolean :delete, :default => false
    end
  end

  def down
    drop_table :features
  end
end
