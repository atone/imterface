class AddSellerUrlToApps < ActiveRecord::Migration
  def up
    add_column :apps, :sellerUrl, :string
  end

  def down
    remove_column :apps, :sellerUrl
  end
end
