class CreateHistory < ActiveRecord::Migration
  def up
    create_table :histories do |t|
      t.string :keyword
      
      t.timestamps
    end
  end

  def down
    drop_table :histories
  end
end
