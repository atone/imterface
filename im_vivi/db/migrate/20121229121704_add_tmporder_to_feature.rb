class AddTmporderToFeature < ActiveRecord::Migration
  def up
  	add_column :features, :tmporder, :integer, :default => 0
  end

  def down
  	remove_column :features, :tmporder
  end
end
