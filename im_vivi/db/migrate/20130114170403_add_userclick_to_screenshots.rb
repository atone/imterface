class AddUserclickToScreenshots < ActiveRecord::Migration
  def up
  	add_column :screenshots, :userclick, :integer, :default => 0
  end

  def down
  	remove_column :screenshots, :userclick
  end
end
