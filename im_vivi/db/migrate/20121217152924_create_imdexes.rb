class CreateImdexes < ActiveRecord::Migration
  def up
    create_table :imdexes do |t|
      t.integer :screenshot_id
      t.string  :word
      t.integer :weight
      t.timestamps
    end
  end

  def down
    drop_table :imdexes
  end
end
