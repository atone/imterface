class AddVisiableToFeature < ActiveRecord::Migration
  def up
            add_column :features, :visiable, :boolean, :default => true
  end

  def down
            remove_column :features, :visiable
  end
end
