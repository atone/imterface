class AddVersionToScreenshots < ActiveRecord::Migration
  def up
  	add_column :screenshots, :version, :string
  end

  def down
  	remove_column :screenshots, :version
  end
end
