class CreateSynonyms < ActiveRecord::Migration
  def up
    create_table :synonyms do |t|
      t.integer :group_id
      t.string  :word
    end

  end

  def down
	drop_table :synonyms
  end
end
