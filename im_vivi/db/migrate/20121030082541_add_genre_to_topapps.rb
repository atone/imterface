class AddGenreToTopapps < ActiveRecord::Migration
  def up
    add_column :topapps, :genre, :string, :default => "all"
  end

  def down
    remove_column :topapps, :genre
  end
end
