class CreateScreenshot < ActiveRecord::Migration
  def up
    create_table :screenshots do |t|
      t.integer :app_id
      t.string  :url
    end
    
    create_table :screenshotfeatures do |t|
      t.integer :screenshot_id
      t.integer :feature_id
    end
  end

  def down
    drop_table :screenshots
    drop_table :screenshotfeatures
  end
end
