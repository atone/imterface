class CreateTopapps < ActiveRecord::Migration
  def up
    create_table :topapps do |t|
      t.integer :app_id
      t.integer :top_id
      t.integer :seq
    end
  end

  def down
    drop_table :topapps
  end
end
