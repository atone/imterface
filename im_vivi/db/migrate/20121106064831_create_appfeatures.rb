class CreateAppfeatures < ActiveRecord::Migration
  def up
    create_table :appfeatures do |t|
      t.integer :app_id
      t.integer :feature_id
      t.string  :url
      t.boolean :isIphone, :default => true
    end
  end

  def down
    drop_table :appfeatures
  end
end
