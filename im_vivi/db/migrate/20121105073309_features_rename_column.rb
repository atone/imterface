class FeaturesRenameColumn < ActiveRecord::Migration
  def up
    rename_column :features, :delete, :purge
  end

  def down
    rename_column :features, :purge, :delete
  end
end
