require 'sinatra/activerecord'

require 'squeel'
require 'meta_search'

DB_CONFIG = YAML::load(File.open('config/database_prod.yml'))
set :database, "mysql2://#{DB_CONFIG['username']}:#{DB_CONFIG['password']}@#{DB_CONFIG['host']}:#{DB_CONFIG['port']}/#{DB_CONFIG['database']}?pool=15"

# class User < ActiveRecord::Base
# end

# ActiveRecord::Base.logger.level = Logger::INFO

class App < ActiveRecord::Base
  # default_scope order('averageUserRating DESC')
  
  validates :trackId, :uniqueness => true
  # has_many :topapps
  # has_many :tops, :through => :topapps
  
  has_many :screenshots
  
  # 命名衝突 App 欄位 features 
  # has_many :appfeatures
  # has_many :features, :through => :appfeatures
end

# class Top < ActiveRecord::Base
#   has_many :topapps
#   has_many :apps, :through => :topapps
# end

# class Topapp < ActiveRecord::Base
#   default_scope order('seq')
  
#   belongs_to :app
#   belongs_to :top
# end

class Feature < ActiveRecord::Base
  # default_scope order('seq')
  # default_scope where(:visiable => true)
  scope :sort_by_seq, order('seq')
  scope :available,   where(:purge => false, :visiable => true)
  scope :not_delete,  where(:purge => false)
  # has_many :appfeatures
  # has_many :apps, :through => :appfeatures
  
  has_many :screenshotfeatures, :dependent => :delete_all
  has_many :screenshots, :through => :screenshotfeatures
end

# class Appfeature < ActiveRecord::Base
#   belongs_to :app
#   belongs_to :feature
# end

class Screenshot < ActiveRecord::Base
  belongs_to :app
  
  has_many :screenshotfeatures
  has_many :features, :through => :screenshotfeatures
end

class Screenshotfeature < ActiveRecord::Base
  belongs_to :screenshot
  belongs_to :feature
end

class History < ActiveRecord::Base
end

class Synonym < ActiveRecord::Base
  validates :word, :uniqueness => true
end

class Imdex < ActiveRecord::Base
  belongs_to :screenshot
end
