# encoding: UTF-8

def search_api(query)
  search_api = 'http://itunes.apple.com/search?entity=software&limit=10&term=' + URI.escape( query.split(/\s+/).join("+") )
    @json = JSON.parse(RestClient.get(search_api))
  
    trackIds = []
    ActiveRecord::Base.transaction do
        @json["results"].each do |json_app|
          trackIds << json_app["trackId"].to_s
          
          app = App.where{trackId==json_app["trackId"].to_s}.first
          
          if app.nil?
            app = App.create(
                          :kind => json_app["kind"],
                          :features => json_app["features"].join(","),
                          :supportedDevices => json_app["supportedDevices"].join(","),
                          :isGameCenterEnabled => json_app["isGameCenterEnabled"] ? "t" : "f",
                          :artistViewUrl => json_app["artistViewUrl"],
                          :artworkUrl60 => json_app["artworkUrl60"],
                          :screenshotUrls => json_app["screenshotUrls"].join(","),
                          :ipadScreenshotUrls => json_app["ipadScreenshotUrls"].join(","),
                          :artworkUrl512 => json_app["artworkUrl512"],
                          :artistId => json_app["artistId"].to_s,
                          :artistName => json_app["artistName"],
                          :price => json_app["price"].to_s,
                          :version => json_app["version"],
                          :description => json_app["description"],
                          :genreIds => json_app["genreIds"].join(","),
                          :releaseDate => json_app["releaseDate"],
                          :sellerName => json_app["sellerName"],
                          :currency => json_app["currency"],
                          :genres => json_app["genres"].join(","),
                          :bundleId => json_app["bundleId"],
                          :trackId => json_app["trackId"].to_s,
                          :trackName => json_app["trackName"],
                          :primaryGenreName => json_app["primaryGenreName"],
                          :primaryGenreId => json_app["primaryGenreId"].to_s,
                          :releaseNotes => json_app["releaseNotes"],
                          :wrapperType => json_app["wrapperType"],
                          :trackCensoredName => json_app["trackCensoredName"],
                          :trackViewUrl => json_app["trackViewUrl"],
                          :contentAdvisoryRating => json_app["contentAdvisoryRating"],
                          :artworkUrl100 => json_app["artworkUrl100"],
                          :languageCodesISO2A => json_app["languageCodesISO2A"].join(","),
                          :fileSizeBytes => json_app["fileSizeBytes"],
                          :formattedPrice => json_app["formattedPrice"],
                          :averageUserRatingForCurrentVersion => json_app["averageUserRatingForCurrentVersion"].to_s,
                          :userRatingCountForCurrentVersion => json_app["userRatingCountForCurrentVersion"].to_s,
                          :trackContentRating => json_app["trackContentRating"],
                          :averageUserRating => json_app["averageUserRating"].to_s,
                          :userRatingCount => json_app["userRatingCount"].to_s,
                          :sellerUrl => json_app["sellerUrl"]
                    )
            
            # puts "create #{app.trackId}"
          end # if App.where{trackId==json_app["trackId"].to_s}.first.nil?
        
          # 檢查每個 App 的 Screenshot 有無新增
          json_app["screenshotUrls"].each do |url|
             #s = Screenshot.new
             #s.url = url
             #s.app_id = app.id
             #s.save
             Screenshot.where(:url => url).where(:app_id => app.id).first_or_create
             # puts "add Screenshot #{url}"
           end # json_app["screenshotUrls"].each do |url|
        
      end # @json["results"].each do |json_app|
    end  # ActiveRecord::Base.transaction do
  
  trackIds
end

get '/test/search_api' do
  query = 'facebook messenger'
  
  search_api(query)
  
  "OK"
end
