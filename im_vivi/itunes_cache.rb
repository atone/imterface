# encoding: UTF-8
require 'sinatra'
require 'rest-client' 
require 'json'
require 'uri'
require 'pry'
require 'dalli'

require './database.rb'
require './search_api.rb'
#require './synonyms.rb'
#require './backstage.rb'

use Rack::Session::Pool, :expire_after => 2592000

#set :environment, :production

#ActiveRecord::Base.logger.level = Logger::INFO

#use ActiveRecord::QueryCache

$USE_CACHE = false

ep = "dbcache.bt02g8.cfg.use1.cache.amazonaws.com:11211"
$dc = Dalli::Client.new( ep )
#dc.stats.first[1]

get '/test' do
  10.times{ Feature.first }
end

get '/home' do
  session[:features] = nil
  
  @search = ""
  @features = []
  @screenshots = []
  @f_count = {}
  @sc_count = 0

  erb :home, :layout => "<%= yield %>"
end

get '/' do
  session[:features] = nil
  
  @search = ""
  @features = []
  @screenshots = []
  @f_count = {}
  @sc_count = 0
  
  # erb :ux, :layout => :layout_ux
  erb :home, :layout => "<%= yield %>"
end

# *** imterface 搜尋進入點 ***
post '/' do
  @search = params[:search]
  @screenshots = []
  @f_count = {}

  # 上次 跟 本次 搜尋條件不同 則重新查詢
  if session[:search] != nil && session[:search] != @search
    session[:features] = nil
  end

  # 儲存搜尋條件
  session[:search] = @search

  cached_result = $dc.get( @search.strip.downcase )
  if cached_result.nil?

    # LEFT JOIN FEATURES
    fjoin = 'LEFT JOIN screenshotfeatures ON screenshotfeatures.screenshot_id = screenshots.id LEFT JOIN features ON features.id = screenshotfeatures.feature_id'

    # 關鍵字過濾
    ss_ids=[]

    # 針對每個關鍵字循環查詢
    search_arr = @search.split(/[,]+\s*/) # "abc,def ghi, jkl" => ["abc", "def ghi", "jkl"]
    search_arr.each_with_index do | word, index|
      rel = Screenshot.joins(:app, :features).search(:app_trackName_or_app_description_or_features_name_or_note_contains => word, :app_primaryGenreId_does_not_equal => "6014")
      
      if index==0
        # 第一次搜尋
        ss_ids = rel.relation.select("DISTINCT screenshots.id").pluck("screenshots.id")

        # 測試：如果查無資料，透過 SEARCH API 查詢，然後再重查一次
        if ss_ids.empty?
          trackIds = search_api(word)
          ss_ids = Screenshot.joins(:app).where("trackId in (?) and primaryGenreId!= '6014' ", trackIds).pluck("screenshots.id")
        end
      else
        # 第 N 次搜尋
        ss_ids = rel.relation.select("DISTINCT screenshots.id").where("screenshots.id in (?)", ss_ids).pluck("screenshots.id")
      end
    end


    # 關連性 準備
    @screenshots = []
    sarr = search_arr.collect(&:downcase)
    # relation = Screenshot.select("DISTINCT screenshots.id, screenshots.*").where("screenshots.id in (?)", ss_ids)

    # Eager Loading
    result = Screenshot.includes(:app, :features).select("DISTINCT screenshots.id, screenshots.*").where("screenshots.id in (?)", ss_ids).order('apps.averageUserRating DESC').all
    

    # 美觀度排序
    s_0 = sort_beautiful(result)

    # 關連性 過濾 >>>>>
    collect_ss = []

    # User Click (>3)
    s_0, collect_ss = sort_user_click(s_0, collect_ss, sarr)

     # [搜尋結果排序] Feature Name 完全符合搜尋關鍵字。
    s_0, collect_ss = sort_feature_name(s_0, collect_ss, sarr)

    # editor 的 Note
    s_0, collect_ss = sort_screenshot_note(s_0, collect_ss, sarr)

    # [搜尋結果排序]AppName 完全符合搜尋關鍵字。
    s_0, collect_ss = sort_app_name_full(s_0, collect_ss, sarr)

    # [搜尋結果排序]AppName 前面符合搜尋關鍵字。
    s_0, collect_ss = sort_app_name_start(s_0, collect_ss, sarr)


    # 關連性 過濾 <<<<<
    @screenshots = collect_ss + s_0

    $dc.set(@search.strip.downcase, @screenshots)
    $dc.set(@search.strip.downcase+'_ss_ids', ss_ids)

    # 總筆數
    @sc_count = @screenshots.size

    @screenshots = @screenshots[0..20]

    # 去除橫幅
    # @screenshots = remove_landscape( @screenshots )



    # 每個 Feature 筆數
    @f_count =  Screenshot.joins(:features).select("features.id, count(features.id) as count").where("screenshots.id in (?)", ss_ids).group("features.id").count
    # @f_count = {}

  # 過濾不顯示的 Features
  @features = Feature.available.sort_by_seq.where("id in (?)", @f_count.keys).all
  
  # Log
  ActiveRecord::Base.transaction do
    History.create(:keyword => @search.strip)
  end

else
  @screenshots = cached_result
  @sc_count = @screenshots.size
  @screenshots = @screenshots[0..20]
  ss_ids = $dc.get(@search.strip.downcase+'_ss_ids')
  @f_count =  Screenshot.joins(:features).select("features.id, count(features.id) as count").where("screenshots.id in (?)", ss_ids).group("features.id").count
  @features = Feature.available.sort_by_seq.where("id in (?)", @f_count.keys).all
end
  
  # 無資料測試
  # @screenshots = []

  # 查無資料顯示
  erb :ux, :layout => :layout_ux
end

def sort_screenshot_note(s_0, collect_ss, sarr)

    s_0.each_with_index do |s,i|
      sarr.each { |word| 
        if s.note!=nil && s.note.downcase.include?( word )
          collect_ss << s
          next
        end
       }
    end

    s_0 = s_0 - collect_ss

    return s_0, collect_ss
end

def sort_app_name_start(s_0, collect_ss, sarr)

    s_0.each_with_index do |s,i|
      if s.app.trackName.downcase.start_with?( *sarr )

         collect_ss << s
      end
    end

    s_0 = s_0 - collect_ss  
    return s_0, collect_ss
end

# 去除橫幅
def remove_landscape(s_0)
    collect_remove = []

    s_0.each_with_index do |s,i|
      width, height = FastImage.size( s.url )

      if width > height
        collect_remove << s
      end
    end

    s_0 = s_0 - collect_remove  
    return s_0
end

def sort_app_name_full(s_0, collect_ss, sarr)

    s_0.each do |s|
      app_name = s.app.trackName.downcase

      collect_ss << s if sarr.include?( app_name )
    end

    s_0 = s_0 - collect_ss

    return s_0, collect_ss
end

def sort_feature_name(s_0, collect_ss, sarr)
     # [搜尋結果排序] Feature Name 完全符合搜尋關鍵字。
    s_0.each do |s|
      f_names = s.features.collect(&:name).collect(&:downcase)

      if (sarr & f_names).size > 0 
        collect_ss << s
      end
    end
    s_0 = s_0 - collect_ss

    return s_0, collect_ss
end

# User Click
def sort_user_click(s_0, collect_ss, sarr)
     # [搜尋結果排序] Feature Name 完全符合搜尋關鍵字。
    s_0.each do |s|
      if s.userclick > 3
        collect_ss << s
      end
    end

    s_0 = s_0 - collect_ss

    return s_0, collect_ss.sort {|x,y| y.userclick <=> x.userclick }
end

def sort_beautiful(result)
    s_0 = []
    s_0_b3 = []
    s_0_b2 = []
    s_0_b1 = []
    s_0_b0 = []

    result.each_with_index do |s,i|

      f_names = s.features.collect(&:name)

      if f_names.include?("★★★")
        s_0_b3 << s
        next
      end

      if f_names.include?("★★")
        s_0_b2 << s
        next
      end

      if f_names.include?("★")
        s_0_b1 << s
        next
      end

      s_0_b0 << s
    end

    s_0 << s_0_b3
    s_0 << s_0_b2
    s_0 << s_0_b1
    s_0 << s_0_b0
    s_0.flatten!
end

# 彈出 App 資訊
get '/uxinfo/:screenshot_id' do
  @screenshot = Screenshot.find params[:screenshot_id]
  @app = @screenshot.app
  
  ActiveRecord::Base.transaction do
    @screenshot.userclick = @screenshot.userclick + 1
    @screenshot.save
  end

  erb :uxinfo, :layout => "<%= yield %>"
end


# 搜尋記錄
get '/history' do
  html = "<table>"
  History.all.each do |h|
    
    html = html + "<tr><td>#{h.keyword}</td><td>#{h.created_at.getlocal("+08:00").strftime("%F %T")}</td></tr>"
  end
  html = html + "</table>"
  html
end
