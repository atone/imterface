# encoding: UTF-8
require 'bundler'
Bundler.require

# require 'sinatra'
# require 'rest-client' 
# require 'json'
# require 'uri'
# require 'pry'

require './database.rb'
require './search_api.rb'
require './cached.rb'
#require './synonyms.rb'
#require './backstage.rb'



# logger = Logger.new('log/im.log')
# logger.level  = Logger::INFO 
# use Rack::CommonLogger, logger

# logger = File.new("log/im.log", "a")
# use Rack::CommonLogger, logger
# STDOUT.reopen(logger)
# STDERR.reopen(logger)

use Rack::Session::Pool
# use Rack::Session::Pool, :expire_after => 2592000

# use Rack::Session::Cookie

 set :environment, :production
# helpers do
#   def logger
#     request.logger
#   end
# end


get '/home' do
  # session[:features] = nil
  
  @search = ""
  @features = []
  @screenshots = []
  @f_count = {}
  @sc_count = 0

  erb :home, :layout => "<%= yield %>"
end

get '/' do
  # binding.pry
  puts ("logger 222 " + Time.now.to_s)

  # session[:features] = nil
  session[:screenshots] = nil
  
  @search = ""
  @features = []
  @screenshots = []
  @f_count = {}
  @sc_count = 0

  # @screenshots = Screenshot.limit(10).all
  
  # erb :ux, :layout => :layout_ux
  erb :home, :layout => "<%= yield %>"
end

get '/rest' do
  # puts "LOAD REST"

  if session[:screenshots].nil?
    @screenshots = []
  else
    tmp_ss = session[:screenshots]
    @screenshots = session[:screenshots][0..20]
    session[:screenshots] = tmp_ss - @screenshots


  end

  # puts "LOAD REST #{ @screenshots.size }"
  response.headers['timestamp'] = "#{Time.now.to_i}"
  erb :_rest, :layout => "<%= yield %>"
end

# *** imterface 搜尋進入點 ***
post '/' do
  session[:screenshots] = nil

  @search = params[:search]
  @screenshots = []
  @f_count = {}

  # 上次 跟 本次 搜尋條件不同 則重新查詢
  if session[:search] != nil && session[:search] != @search
    session[:features] = nil
  end

  # 儲存搜尋條件
  session[:search] = @search


  # 是否使用 CACHE
  if $USE_CACHE==false
    cached_result = nil
  else
    cached_result = $dc.get( @search.strip.downcase )
  end
  
  # 如果沒有 CACHE 過
  if cached_result.nil?

    # LEFT JOIN FEATURES
    fjoin = 'LEFT JOIN screenshotfeatures ON screenshotfeatures.screenshot_id = screenshots.id LEFT JOIN features ON features.id = screenshotfeatures.feature_id'

    # 關鍵字過濾
    ss_ids=[]

    # 針對每個關鍵字循環查詢
    
    search_arr = @search.split(/[,]+\s*/) # "abc,def ghi, jkl" => ["abc", "def ghi", "jkl"]
    search_arr.each_with_index do | word, index|
      rel = Screenshot.joins(:app).search(:app_trackName_or_app_description_or_app_sellerName_or_note_contains => word, :app_primaryGenreId_does_not_equal => "6014")
      
      if index==0
        # 第一次搜尋
        ss_ids = rel.relation.select("DISTINCT screenshots.id").pluck("screenshots.id")

        # 測試：如果查無資料，透過 SEARCH API 查詢，然後再重查一次
        if ss_ids.empty?
          trackIds = search_api(word)
          ss_ids = Screenshot.joins(:app).where("trackId in (?) and primaryGenreId!= '6014' ", trackIds).pluck("screenshots.id")
        end
      else
        # 第 N 次搜尋
        ss_ids = rel.relation.select("DISTINCT screenshots.id").where("screenshots.id in (?)", ss_ids).pluck("screenshots.id")
      end
    end


    # 關連性 準備
    @screenshots = []
    sarr = search_arr.collect(&:downcase)
    # relation = Screenshot.select("DISTINCT screenshots.id, screenshots.*").where("screenshots.id in (?)", ss_ids)

    # Eager Loading
    result = Screenshot.includes(:app).select("DISTINCT screenshots.id, screenshots.*").where("screenshots.id in (?)", ss_ids).order('apps.averageUserRating DESC').all # order('apps.averageUserRating DESC')


    # 美觀度排序
    # s_0 = sort_beautiful(result)
    s_0 = result

    # 關連性 過濾 >>>>>
    collect_ss = []


     # [搜尋結果排序] Feature Name 完全符合搜尋關鍵字。
    # s_0, collect_ss = sort_feature_name(s_0, collect_ss, sarr)

    # editor 的 Note
    s_0, collect_ss = sort_screenshot_note(s_0, collect_ss, sarr)

    # User Click (>3)
    s_0, collect_ss = sort_user_click(s_0, collect_ss, sarr)

    # [搜尋結果排序]AppName 完全符合搜尋關鍵字。
    s_0, collect_ss = sort_app_name_full(s_0, collect_ss, sarr)

    # [搜尋結果排序]AppName 前面符合搜尋關鍵字。
    s_0, collect_ss = sort_app_name_start(s_0, collect_ss, sarr)

    # 開發商
    s_0, collect_ss = sort_seller_name(s_0, collect_ss, sarr)

    # 關連性 過濾 <<<<<
    @screenshots = collect_ss + s_0[0..20]

    # CACHE 結果
    if $USE_CACHE==true
      $dc.set(@search.strip.downcase, @screenshots)
      $dc.set(@search.strip.downcase+'_ss_ids', ss_ids)
    end

    # 總筆數
    @sc_count = @screenshots.size

    tmp_ss = @screenshots
    @screenshots = @screenshots[0..20]
    session[:screenshots] = tmp_ss - @screenshots

    # 去除橫幅
    # @screenshots = remove_landscape( @screenshots )



    # 每個 Feature 筆數
    # @f_count =  Screenshot.joins(:features).select("features.id, count(features.id) as count").where("screenshots.id in (?)", ss_ids).group("features.id").count
    # @f_count = {}

  # 過濾不顯示的 Features
  # @features = Feature.available.sort_by_seq.where("id in (?)", @f_count.keys).all
  
else # cached_result.nil?
  # 使用 CACHE 結果
  @screenshots = cached_result
  ss_ids = $dc.get(@search.strip.downcase+'_ss_ids')
  @sc_count = @screenshots.size

  tmp_ss = @screenshots
  @screenshots = @screenshots[0..20]
  session[:screenshots] = tmp_ss - @screenshots
  
  # @f_count =  Screenshot.joins(:features).select("features.id, count(features.id) as count").where("screenshots.id in (?)", ss_ids).group("features.id").count
  # @features = Feature.available.sort_by_seq.where("id in (?)", @f_count.keys).all
end # cached_result.nil?

  # Log
  

  ActiveRecord::Base.transaction do
    real_ip = @env['HTTP_X_REAL_IP'] ||= @env['REMOTE_ADDR']
    h = History.create(
      :keyword => @search.strip, 
      :ip => real_ip, 
      :ssids => ""
      )
    session[:log] = h.id
  end

  # 無資料測試
  # @screenshots = []

  

  # 查無資料顯示
  erb :ux, :layout => "<%= yield %>"
end

def sort_seller_name(s_0, collect_ss, sarr)

    s_0.each do |s|
      sellerName = s.app.sellerName.downcase

      collect_ss << s if sarr.include?( sellerName )
    end

    s_0 = s_0 - collect_ss

    return s_0, collect_ss
end

def sort_screenshot_note(s_0, collect_ss, sarr)

    s_0.each_with_index do |s,i|
      sarr.each { |word| 
        if s.note!=nil && s.note.downcase.include?( word )
          collect_ss << s
          next
        end
       }
    end

    s_0 = s_0 - collect_ss

    return s_0, collect_ss
end

def sort_app_name_start(s_0, collect_ss, sarr)

    s_0.each_with_index do |s,i|
      if s.app.trackName.downcase.start_with?( *sarr )

         collect_ss << s
      end
    end

    s_0 = s_0 - collect_ss  
    return s_0, collect_ss
end

# 去除橫幅
def remove_landscape(s_0)
    collect_remove = []

    s_0.each_with_index do |s,i|
      width, height = FastImage.size( s.url )

      if width > height
        collect_remove << s
      end
    end

    s_0 = s_0 - collect_remove  
    return s_0
end

def sort_app_name_full(s_0, collect_ss, sarr)

    s_0.each do |s|
      app_name = s.app.trackName.downcase

      collect_ss << s if sarr.include?( app_name )
    end

    s_0 = s_0 - collect_ss

    return s_0, collect_ss
end

def sort_feature_name(s_0, collect_ss, sarr)
     # [搜尋結果排序] Feature Name 完全符合搜尋關鍵字。
    s_0.each do |s|
      f_names = s.features.collect(&:name).collect(&:downcase)

      if (sarr & f_names).size > 0 
        collect_ss << s
      end
    end
    s_0 = s_0 - collect_ss

    return s_0, collect_ss
end

# User Click
def sort_user_click(s_0, collect_ss, sarr)
     # [搜尋結果排序] Feature Name 完全符合搜尋關鍵字。
    s_0.each do |s|
      if s.userclick > 3
        collect_ss << s
      end
    end

    s_0 = s_0 - collect_ss

    return s_0, collect_ss.sort {|x,y| y.userclick <=> x.userclick }
end

def sort_beautiful(result)
    s_0 = []
    s_0_b3 = []
    s_0_b2 = []
    s_0_b1 = []
    s_0_b0 = []

    result.each_with_index do |s,i|

      f_names = s.features.collect(&:name)

      if f_names.include?("★★★")
        s_0_b3 << s
        next
      end

      if f_names.include?("★★")
        s_0_b2 << s
        next
      end

      if f_names.include?("★")
        s_0_b1 << s
        next
      end

      s_0_b0 << s
    end

    s_0 << s_0_b3
    s_0 << s_0_b2
    s_0 << s_0_b1
    s_0 << s_0_b0
    s_0.flatten!
end

def numeric?(object)
  true if Float(object) rescue false
end

# 彈出 App 資訊
get '/uxinfo/:screenshot_id' do
  @screenshot = Screenshot.find params[:screenshot_id]
  @app = @screenshot.app
  
  ActiveRecord::Base.transaction do
    @screenshot.userclick = @screenshot.userclick + 1
    @screenshot.save

    # 有時候畫面停住 session 會 timeout
    if session[:log]!=nil?
      if numeric?( session[:log] )

        h = History.find session[:log]

        unless h.nil?
          if h.ssids.nil?
            h.ssids = ""
          end
          h.ssids = h.ssids + "#{params[:screenshot_id]},"
          h.save
          
        end

        # session[:log] = h.id
      end
    end

  end

  erb :uxinfo, :layout => "<%= yield %>"
end


# 搜尋記錄
get '/history' do
  html = "<table>"
  History.all.each do |h|
    ssids = []
    if h.ssids!=nil
      ssids = h.ssids.split(",")
    end
    html = html + "<tr><td><a href='/history/#{h.id}'>#{h.keyword}</a></td><td>#{ssids.size} screenshots</td><td>#{h.created_at.getlocal("+08:00").strftime("%F %T")}</td></tr>"
  end
  html = html + "</table>"
  html
end

get '/history/:id' do
  h = History.find params[:id]
  html = "<p>#{h.keyword} from #{h.ip}</p>"
  screenshots = []

  if h.ssids!=nil
    # screenshots = Screenshot.where("id in (?)", h.ssids.split(",") )

    html += "<table>"
    html += "<tr>"

    ssids = h.ssids.split(",")
    ssids.each { |sid| 
      s = Screenshot.find sid
      # html += "<span><img src='#{s.url}' style='width:320px; height:480px;'></span>&nbsp;"
      html += "<td><img src='#{s.url}' style='width:320px; height:480px;'></td>"
    }

    html += "</tr> <tr>"

    ssids.each { |sid| 
      s = Screenshot.find sid
      html += "<td>#{s.note}</td>"
    }

    html += "</tr> </table>"
  end
  
  # binding.pry

  # h.ssids.split(",").each { |sid|  
  #   screenshots.each { |s|  
  #     if s.id == sid
  #       html += "<span><img src='#{s.url}' style='width:320px; height:480px;'></span>&nbsp;"
  #       break
  #     end
  #   }
  # }


  html
end
